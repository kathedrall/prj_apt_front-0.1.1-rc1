import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { Car } from '../car';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  cars: Car[];
  heroService: HeroService;

  constructor(heroService: HeroService) {
    this.heroService = heroService;
  }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes().subscribe(async (res) => {
      this.cars = res;
    });
  }

  add(model: string,make: string,year: string): void {
    model = model.trim();

    if (!model) { return; }
    this.heroService.addCar({ model,make,year } as Car)
      .subscribe(car => {
        this.cars.push(car);
      });
  }

  delete(car: Car): void {
    
    this.cars = this.cars.filter(h => h !== car);

    this.heroService.deleteCar(car).subscribe();
  }

}