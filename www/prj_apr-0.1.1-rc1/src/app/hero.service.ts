import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Car } from './car';
import { MessageService } from './message.service';

const httpOptions = {
	headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
      'Access-Control-Allow-Headers':
        'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
    })
};

@Injectable({ providedIn: 'root' })
export class HeroService {

	// private heroesUrl = 'api/heroes';  // URL to web api
	private heroesUrl = 'http://localhost:8080/blog/public/api/v1/car';  // URL to web api
	// private heroesUrl = 'http://localhost:8084/carros';  // URL to web api

	constructor(
		private http: HttpClient,
		private messageService: MessageService) { }

	getHeroes (): any {
		return this.http.get(this.heroesUrl);		
	}

	addCar (car: Car): Observable<Car> {
		return this.http.post<Car>(this.heroesUrl, car, httpOptions).pipe(
			tap((car: Car) => this.log(`added car w/ id=${car.id}`)),
			catchError(this.handleError<Car>('addCar'))
			);
	}

	deleteCar (car: Car | number): Observable<Car> {
		const id = typeof car === 'number' ? car : car.id;
		const url = `${this.heroesUrl}/${id}`;

		return this.http.delete<Car>(url, httpOptions).pipe(
			tap(_ => this.log(`deleted car id=${id}`)),
			catchError(this.handleError<Car>('deleteCar'))
			);
	}

	getCar(id: number): Observable<Car> {
		const url = `${this.heroesUrl}/${id}`;
		return this.http.get<Car>(url).pipe(
			tap(_ => this.log(`fetched hero id=${id}`)),
			catchError(this.handleError<Car>(`getCar id=${id}`))
			);
	}

	updateCar (car: Car): Observable<any> {

		const id = car.id;
		const url = `${this.heroesUrl}/${id}`;
		return this.http.put(url, car, httpOptions).pipe(
			tap(_ => this.log(`updated car id=${car.id}`)),
			catchError(this.handleError<any>('updateCar'))
			);
	}

	/**
	* Handle Http operation that failed.
	* Let the app continue.
	* @param operation - name of the operation that failed
	* @param result - optional value to return as the observable result
	*/
	private handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log(`${operation} failed: ${error.message}`);

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a HeroService message with the MessageService */
	private log(message: string) {
		this.messageService.add(`HeroService: ${message}`);
	}

	/** GET heroes from the server */
	// getHeroes (): Observable {

	// 	return this.http.get(this.heroesUrl)
	// 	.pipe(
	// 		tap(heroes => this.log('fetched cars')),
	// 		catchError(this.handleError('getHeroes', []))
	// 		);
	// }

	// /** GET hero by id. Return `undefined` when id not found */
	// getHeroNo404<Data>(id: number): Observable<Hero> {
	// 	const url = `${this.heroesUrl}/?id=${id}`;
	// 	return this.http.get<Hero[]>(url)
	// 	.pipe(
	// 	      map(heroes => heroes[0]), // returns a {0|1} element array
	// 	      tap(h => {
	// 	      	const outcome = h ? `fetched` : `did not find`;
	// 	      	this.log(`${outcome} hero id=${id}`);
	// 	      }),
	// 	      catchError(this.handleError<Hero>(`getHero id=${id}`))
	// 	      );
	// }

	// /* GET heroes whose name contains search term */
	// searchHeroes(term: string): Observable<Hero[]> {
	// 	if (!term.trim()) {
	// 	    // if not search term, return empty hero array.
	// 	    return of([]);
	// 	}

	// 	return this.http.get<Hero[]>(`${this.heroesUrl}/?name=${term}`).pipe(
	// 		tap(_ => this.log(`found heroes matching "${term}"`)),
	// 		catchError(this.handleError<Hero[]>('searchHeroes', []))
	// 		);
	// }

}