import { Component, OnInit, Input } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Hero }         from '../hero';
import { Car }         from '../car';
import { HeroService }  from '../hero.service';

@Component({
	selector: 'app-hero-detail',
	templateUrl: './hero-detail.component.html',
	styleUrls: [ './hero-detail.component.css' ]
})
export class HeroDetailComponent implements OnInit {
	@Input() car: Car;

	constructor(
		private route: ActivatedRoute,
		private heroService: HeroService,
		private location: Location,
		private router: Router,
		) {}

	ngOnInit(): void {
		this.getCar();
	}

	getCar(): void {
		const id = +this.route.snapshot.paramMap.get('id');

		this.heroService.getCar(id)
		.subscribe(car => this.car = car);
	}

	goBack(): void {
		this.location.back();
	}

	save(): void {    
		this.heroService.updateCar(this.car)
		.subscribe(() => this.goBack());
	}

	delete(car: Car): void {
		this.heroService.deleteCar(car).subscribe();
		this.router.navigate(['/cars']);
	}	
}